package hnweb.com.ekasst.utils;

/**
 * Created by neha on 6/26/2017.
 */

public class AppAPI {

    public static String BASE_URL = "http://104.37.185.20/~tech599/tech599.com/johnman/eksatt/api/";

    public static String REGISTER_URL = BASE_URL + "user_registration.php";
    public static String LOGIN_URL = BASE_URL + "login.php";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword.php";
    public static String GET_PROFILE = BASE_URL + "get_profile.php";
    public static String EDIT_PROFILE = BASE_URL + "edit_profile.php";

    public static String GET_BOOK_STORE = BASE_URL + "bookstore.php";
    public static String GET_CATEGORY_LIST = BASE_URL + "get_all_book_categories.php";
    public static String SEARCH_A_BOOK = BASE_URL + "search_books.php";
    public static String GET_BOOK_DETAILS = BASE_URL + "get_book_details.php";
    public static String BOOK_BUY_NOW = BASE_URL + "book_sale.php";
    public static String CATEGORY_WISE_BOOKS = BASE_URL + "get_categorywise_booklist.php";
    public static String MY_BOOK_SHELF = BASE_URL + "book_shelf.php";

    public static String UPDATE_PROFILE_PICTURE = BASE_URL + "profile_picture.php";
    public static String GET_MY_UPLOADED_BOOK = BASE_URL + "get_uploaded_books_against_author.php";

    public static String UPLOAD_BOOK = BASE_URL + "book_upload_against_author.php";

    public static String FACEBOOK_LOGIN = BASE_URL + "login_verify_facebook.php";

    public static String BOOK_DISPLAY = BASE_URL + "display_book.php";

    public static String NEW_BOOK_UPLOAD = BASE_URL + "book_upload_against_author_pdf_to_image.php";
}
