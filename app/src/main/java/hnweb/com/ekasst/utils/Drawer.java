package hnweb.com.ekasst.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.activity.LoginActivity;
import hnweb.com.ekasst.fragment.Author.UploadBookFragment;
import hnweb.com.ekasst.fragment.Customer.MyBookShelfFragment;
import hnweb.com.ekasst.fragment.Customer.MyProfileFragment;
import hnweb.com.ekasst.fragment.Customer.TheBookStoreFragment;


/**
 * Created by neha on 6/8/2017.
 */

public class Drawer {

    public static void clicked(Activity context, int id) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0);
        switch (id) {
            case R.id.homeTV:
//"
                String type = sharedPreferences.getString("USER_TYPE", "");
                Log.e("TYPE", type);
                if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("Member")) {
                    TheBookStoreFragment tbsf = new TheBookStoreFragment();
                    ((HomeActivity) context).replaceFragment(tbsf);
                } else {

                    ((HomeActivity) context).titleToolLL.setVisibility(View.GONE);
//            toolbar.setTitle("UPLOAD BOOK");
                    UploadBookFragment ubf = new UploadBookFragment();
                    ((HomeActivity) context).replaceFragment(ubf);

                }
                break;

            case R.id.myProfileTV:
                MyProfileFragment scf = new MyProfileFragment();
                ((HomeActivity) context).replaceFragment(scf);
                break;
            case R.id.resumeTV:
                context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
//                MyPerfectResumeFragment mpf = new MyPerfectResumeFragment();
//                ((HomeActivity) context).replaceFragment(mpf);
                break;
            case R.id.quizTV:
                MyBookShelfFragment mbf = new MyBookShelfFragment();
                ((HomeActivity) context).replaceFragment(mbf);
                break;
            case R.id.resourceTV:
//                ResourcesFragment rpf = new ResourcesFragment();
//                ((HomeActivity) context).replaceFragment(rpf);
                break;
            case R.id.settingsTV:
//                SettingsFragment spf = new SettingsFragment();
//                ((HomeActivity) context).replaceFragment(spf);
                break;
            case R.id.logoutTV:
                if (sharedPreferences.getInt("FB_LOGIN", 0) == 1) {
                    LoginManager.getInstance().logOut();
                }
                logout(context);
                break;

            case R.id.editBTN:
                MyProfileFragment mpf = new MyProfileFragment();
                ((HomeActivity) context).replaceFragment(mpf);
                break;
            case R.id.topLL:
                break;

        }

    }

    public static void logout(final Activity context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context,
                        LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                toastDialog(context, "You are logout successfully");
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(context.getPackageName(),
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }
}

