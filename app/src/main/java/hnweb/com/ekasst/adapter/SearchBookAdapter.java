package hnweb.com.ekasst.adapter;

/**
 * Created by neha on 6/30/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.fragment.Customer.BookDetailsFragment;
import hnweb.com.ekasst.models.SearchBook;

/**
 * Created by neha on 6/30/2017.
 */

/**
 * Created by neha on 6/28/2017.
 */

public class SearchBookAdapter extends RecyclerView.Adapter<SearchBookAdapter.SearchBookViewHolder> {
    Activity activity;
    ArrayList<SearchBook> bookStoreArrayList;

    public SearchBookAdapter(Activity activity, ArrayList<SearchBook> bookStoreArrayList) {
        this.activity = activity;
        this.bookStoreArrayList = bookStoreArrayList;

    }

    @Override
    public SearchBookAdapter.SearchBookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.search_book_item, null);
        SearchBookViewHolder dlvh = new SearchBookViewHolder(layoutView);


        return dlvh;
    }

    @Override
    public void onBindViewHolder(SearchBookAdapter.SearchBookViewHolder holder, final int position) {

        holder.nameTV.setText(bookStoreArrayList.get(position).getBook_title());
        holder.priceTV.setText(bookStoreArrayList.get(position).getBook_price());
        if (!TextUtils.isEmpty(bookStoreArrayList.get(position).getBook_photo()))
            Glide.with(activity).load(bookStoreArrayList.get(position).getBook_photo()).into(holder.bookIV);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookDetailsFragment bdf = new BookDetailsFragment();
                ((HomeActivity) activity).replaceFragmentWithParams(bdf, bookStoreArrayList.get(position).getBook_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookStoreArrayList.size();
    }

    public class SearchBookViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, priceTV;
        public ImageView bookIV;

        public SearchBookViewHolder(View itemView) {
            super(itemView);

            bookIV = (ImageView) itemView.findViewById(R.id.bookIV);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);

        }
    }
}

