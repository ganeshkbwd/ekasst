package hnweb.com.ekasst.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.fragment.Customer.BookDetailsFragment;
import hnweb.com.ekasst.fragment.Customer.BookDisplayFragment;
import hnweb.com.ekasst.models.BookStore;

/**
 * Created by neha on 6/28/2017.
 */

public class BookStoreAdapter extends RecyclerView.Adapter<BookStoreAdapter.BookStoreViewHolder> {
    Activity activity;
    ArrayList<BookStore> bookStoreArrayList;
    String comeFrom;
    Bitmap mBitmap;

    public BookStoreAdapter(Activity activity, ArrayList<BookStore> bookStoreArrayList, String comeFrom) {
        this.activity = activity;
        this.bookStoreArrayList = bookStoreArrayList;
        this.comeFrom = comeFrom;

    }

    @Override
    public BookStoreAdapter.BookStoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.book_store_item, null);
        BookStoreViewHolder dlvh = new BookStoreViewHolder(layoutView);

        return dlvh;
    }

    @Override
    public void onBindViewHolder(BookStoreAdapter.BookStoreViewHolder holder, final int position) {

//        if (Integer.parseInt(bookStoreArrayList.get(position).getStatus())==0){
//            holder.itemView.setVisibility(View.GONE);
//        }else {
//            holder.itemView.setVisibility(View.VISIBLE);
//        }

        holder.nameTV.setText(bookStoreArrayList.get(position).getBook_title());
        holder.priceTV.setText("$ " + bookStoreArrayList.get(position).getBook_price());
        if (!TextUtils.isEmpty(bookStoreArrayList.get(position).getBook_photo()))
            Glide.with(activity).load(bookStoreArrayList.get(position).getBook_photo()).override(100, 100).into(holder.bookIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (comeFrom.equalsIgnoreCase("MYBOOKSHELF")) {
                    BookDisplayFragment wf = new BookDisplayFragment();
                    ((HomeActivity) activity).replaceFragmentWithParams(wf, bookStoreArrayList.get(position).getBook_id());
                } else {
                    BookDetailsFragment bdf = new BookDetailsFragment();
                    ((HomeActivity) activity).replaceFragmentWithParams(bdf, bookStoreArrayList.get(position).getBook_id());
                }
            }
        });
        if (comeFrom.equalsIgnoreCase("MYBOOKSHELF")) {
            holder.openBTN.setVisibility(View.VISIBLE);
            holder.priceTV.setVisibility(View.GONE);
        } else {
            holder.openBTN.setVisibility(View.GONE);
            holder.priceTV.setVisibility(View.VISIBLE);
        }

        holder.openBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BookDisplayFragment wf = new BookDisplayFragment();
                ((HomeActivity) activity).replaceFragmentWithParams(wf, bookStoreArrayList.get(position).getBook_id());

            }
        });

    }

    @Override
    public int getItemCount() {
        return bookStoreArrayList.size();
    }

    public class BookStoreViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, priceTV;
        public ImageView bookIV;
        public TextView openBTN;

        public BookStoreViewHolder(View itemView) {
            super(itemView);

            bookIV = (ImageView) itemView.findViewById(R.id.bookIV);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);
            openBTN = (TextView) itemView.findViewById(R.id.openBTN);

        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void pdfRender_(String book_pdf_link) {
        // create a new renderer
        PdfRenderer renderer = null;
        try {
            renderer = new PdfRenderer(getSeekableFileDescriptor(book_pdf_link));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // let us just render all pages
        final int pageCount = renderer.getPageCount();
        for (int i = 0; i < pageCount; i++) {
            PdfRenderer.Page page = renderer.openPage(i);

            // say we render for showing on the screen
            page.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

            // do stuff with the bitmap
            // close the page
            page.close();
        }
        // close the renderer
        renderer.close();
    }

    private ParcelFileDescriptor getSeekableFileDescriptor(String book_pdf_link) {
        ParcelFileDescriptor fd = null;
        try {
            fd = ParcelFileDescriptor.open(new File(book_pdf_link),
                    ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fd;
    }
}