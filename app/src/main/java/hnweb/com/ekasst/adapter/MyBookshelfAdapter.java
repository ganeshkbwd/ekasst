package hnweb.com.ekasst.adapter;

/**
 * Created by neha on 7/1/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.models.BookStore;

/**
 * Created by neha on 6/28/2017.
 */

public class MyBookshelfAdapter extends RecyclerView.Adapter<MyBookshelfAdapter.MyBookshelfViewHolder> {
    Activity activity;
    ArrayList<BookStore> bookStoreArrayList;

    public MyBookshelfAdapter(Activity activity, ArrayList<BookStore> bookStoreArrayList) {
        this.activity = activity;
        this.bookStoreArrayList = bookStoreArrayList;

    }

    @Override
    public MyBookshelfAdapter.MyBookshelfViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.book_shelf_item, null);
        MyBookshelfViewHolder dlvh = new MyBookshelfViewHolder(layoutView);


        return dlvh;
    }

    @Override
    public void onBindViewHolder(MyBookshelfAdapter.MyBookshelfViewHolder holder, final int position) {

        holder.nameTV.setText(bookStoreArrayList.get(position).getBook_title());
        holder.priceTV.setText("OPEN");
        if (!TextUtils.isEmpty(bookStoreArrayList.get(position).getBook_photo()))
            Glide.with(activity).load(bookStoreArrayList.get(position).getBook_photo()).into(holder.bookIV);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BookDetailsFragment bdf = new BookDetailsFragment();
//                ((HomeActivity) activity).replaceFragmentWithParams(bdf, bookStoreArrayList.get(position).getBook_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookStoreArrayList.size();
    }

    public class MyBookshelfViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, priceTV;
        public ImageView bookIV;

        public MyBookshelfViewHolder(View itemView) {
            super(itemView);

            bookIV = (ImageView) itemView.findViewById(R.id.bookIV);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);


        }
    }
}
