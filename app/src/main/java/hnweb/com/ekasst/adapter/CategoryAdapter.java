package hnweb.com.ekasst.adapter;

/**
 * Created by neha on 6/30/2017.
 */

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.fragment.Customer.CategoryWiseBookFragment;
import hnweb.com.ekasst.models.Category;

/**
 * Created by neha on 6/28/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    Activity activity;
    ArrayList<Category> bookStoreArrayList;

    public CategoryAdapter(Activity activity, ArrayList<Category> bookStoreArrayList) {
        this.activity = activity;
        this.bookStoreArrayList = bookStoreArrayList;

    }

    @Override
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.category_item, null);
        CategoryViewHolder dlvh = new CategoryViewHolder(layoutView);


        return dlvh;
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.CategoryViewHolder holder, final int position) {

        holder.nameTV.setText(bookStoreArrayList.get(position).getCat_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoryWiseBookFragment cwf = new CategoryWiseBookFragment();
                ((HomeActivity)activity).replaceFragmentWithParams(cwf,bookStoreArrayList.get(position).getCat_id());
            }
        });
//        holder.priceTV.setText(bookStoreArrayList.get(position).getBook_price());
//        if (!TextUtils.isEmpty(bookStoreArrayList.get(position).getBook_photo()))
//            Glide.with(activity).load(bookStoreArrayList.get(position).getBook_photo()).into(holder.bookIV);

    }

    @Override
    public int getItemCount() {
        return bookStoreArrayList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, priceTV;
        public ImageView bookIV;

        public CategoryViewHolder(View itemView) {
            super(itemView);

            bookIV = (ImageView) itemView.findViewById(R.id.bookIV);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);

        }
    }
}
