package hnweb.com.ekasst.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.utils.ZoomableImageView;

/**
 * Created by neha on 7/21/2017.
 */

public class BookDisplayAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<String> myList;
    String home;


    public BookDisplayAdapter(Activity activity, ArrayList<String> myList, String home) {
        this.activity = activity;
        this.myList = myList;
        this.home = home;

    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.book_display_item, container, false);

        ZoomableImageView imageView = (ZoomableImageView) itemView.findViewById(R.id.image);
        TextView pgnoTV = (TextView) itemView.findViewById(R.id.pgnoTV);

        Glide.with(activity)
                .load(myList.get(position).toString())
                .asBitmap()
                .into(imageView);

        pgnoTV.setText(String.valueOf(position + 1));
//        CurrentPosition = position;

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
