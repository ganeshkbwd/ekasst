package hnweb.com.ekasst.models;

/**
 * Created by neha on 6/28/2017.
 */

public class BookStore {

    String book_id;
    String book_photo;
    String book_title;
    String book_price;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;

    public String getBook_pdf_link() {
        return book_pdf_link;
    }

    public void setBook_pdf_link(String book_pdf_link) {
        this.book_pdf_link = book_pdf_link;
    }

    String book_pdf_link;

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getBook_photo() {
        return book_photo;
    }

    public void setBook_photo(String book_photo) {
        this.book_photo = book_photo;
    }

    public String getBook_title() {
        return book_title;
    }

    public void setBook_title(String book_title) {
        this.book_title = book_title;
    }

    public String getBook_price() {
        return book_price;
    }

    public void setBook_price(String book_price) {
        this.book_price = book_price;
    }


}
