package hnweb.com.ekasst.fragment.Customer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hnweb.com.ekasst.R;

/**
 * Created by neha on 8/18/2017..
 * Developer :- Ganesh Kulkarni.
 */

public class BookStoreParentFragment extends Fragment {
    ViewPager mViewPager;
    boolean visible = false;
    CustomPagerAdapter pagerAdapter;


    public BookStoreParentFragment() {
        super();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_book_store_parent, container, false);
        visible = true;
        init(myFragmentView);

        return myFragmentView;
    }

    public void init(View v) {
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                } else if (position == 1) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.myProfileBTN:
//                mViewPager.setCurrentItem(0);
//                mpTV.setVisibility(View.VISIBLE);
//                cpTV.setVisibility(View.GONE);
////                myProfileClick();
//                break;
//            case R.id.changePasswordBTN:
//                mViewPager.setCurrentItem(1);
//                cpTV.setVisibility(View.VISIBLE);
//                mpTV.setVisibility(View.GONE);
////                changePasswordClick();
//                break;
//        }
//    }

//    public void myProfileClick() {
//        mpTV.setVisibility(View.VISIBLE);
//        cpTV.setVisibility(View.GONE);
//        EditProfileFragment ef = new EditProfileFragment();
//        replaceFragment(ef);
//    }

//    public void changePasswordClick() {
//        cpTV.setVisibility(View.VISIBLE);
//        mpTV.setVisibility(View.GONE);
//        ChangePasswordFragment cf = new ChangePasswordFragment();
//        replaceFragment(cf);
//    }


//    public void replaceFragment(android.support.v4.app.Fragment fragment) {
//        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.frameLayout, fragment);
//        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        fragmentTransaction.addToBackStack(fragment.getClass().getName());
//        fragmentTransaction.commit();
//    }


    public class CustomPagerAdapter extends FragmentPagerAdapter {

        protected Activity mContext;

        public CustomPagerAdapter(FragmentManager fm, Activity context) {
            super(fm);
            mContext = context;
        }


        @Override
        // This method returns the fragment associated with
        // the specified position.
        //
        // It is called when the Adapter needs a fragment
        // and it does not exists.
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    return new TheBookStoreFragment();
                case 1:

                    return new SearchFragment();
                default:

                    return new TheBookStoreFragment();
            }


        }

        @Override
        public int getCount() {
            return 2;
        }

    }

}
