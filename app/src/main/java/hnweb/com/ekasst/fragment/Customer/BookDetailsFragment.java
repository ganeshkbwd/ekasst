package hnweb.com.ekasst.fragment.Customer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.utils.AppAPI;

/**
 * Created by neha on 7/1/2017.
 */

public class BookDetailsFragment extends Fragment {

    ImageView bookIV;
    TextView bookNameTV, authorTV, priceTV, descriptionTV, detailsTV;
    Button buyNowBTN;
    SharedPreferences sharedPreferences;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    String book_id;


    public BookDetailsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_book_details, container, false);

        ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("Book Details");
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        init(myFragmentView);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        book_id = getArguments().getString("BOOKID");
        doGetBookDetails(book_id);
        return myFragmentView;
    }

    public void init(View v) {

        bookIV = (ImageView) v.findViewById(R.id.bookIV);
        bookNameTV = (TextView) v.findViewById(R.id.bookNameTV);
        authorTV = (TextView) v.findViewById(R.id.authorTV);
        priceTV = (TextView) v.findViewById(R.id.priceTV);
        descriptionTV = (TextView) v.findViewById(R.id.descriptionTV);
//        detailsTV = (TextView) v.findViewById(R.id.detailsTV);
        buyNowBTN = (Button) v.findViewById(R.id.buyNowBTN);

        buyNowBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doBookBuyNow(book_id);
            }
        });


    }


    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getBookDetails")) {

                            JSONArray jarr = jobj.getJSONArray("parameters");
                            String book_photo = jarr.getJSONObject(0).getString("book_photo");
                            String book_title = jarr.getJSONObject(0).getString("book_title");
                            String book_description = jarr.getJSONObject(0).getString("book_description");
//                            String publisher_details = jarr.getJSONObject(0).getString("publisher_details");
                            String book_author_name = jarr.getJSONObject(0).getString("book_author_name");
                            String book_price = jarr.getJSONObject(0).getString("book_price");
                            String is_free = jarr.getJSONObject(0).getString("is_free");

                            Glide.with(getActivity()).load(book_photo).into(bookIV);
                            bookNameTV.setText(book_title);
                            descriptionTV.setText(book_description);
                            if (is_free.equalsIgnoreCase("Yes")) {
                                priceTV.setText("FREE");
                            } else {
                                priceTV.setText("$ " + book_price);
                            }
                            authorTV.setText(book_author_name);
//                            detailsTV.setText(publisher_details);

                        } else if (request_tag.equalsIgnoreCase("getBookBuyNow")) {
                            MyBookShelfFragment mbf = new MyBookShelfFragment();
                            ((HomeActivity) getActivity()).replaceFragment(mbf);

                        }


                    }else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void doGetBookDetails(String book_id) {
        Map<String, String> params = new HashMap<>();
        params.put("book_id", book_id);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_BOOK_DETAILS, params, "getBookDetails");
    }

    public void doBookBuyNow(String book_id) {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        params.put("book_id", book_id);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.BOOK_BUY_NOW, params, "getBookBuyNow");
    }

}
