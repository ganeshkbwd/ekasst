package hnweb.com.ekasst.fragment.Customer;

import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;

import hnweb.com.ekasst.R;

/**
 * Created by neha on 7/17/2017.
 */

public class PdfViewFragment extends Fragment implements OnLoadCompleteListener, OnPageChangeListener, OnDrawListener, OnPageScrollListener, OnErrorListener, OnRenderListener {
    private PDFView pdfView;
    String formURL;

    public PdfViewFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_pdf_view, container, false);

        formURL = getArguments().getString("BOOKID");
        pdfView = (PDFView) myFragmentView.findViewById(R.id.pdfView);

        pdfView.fromUri(Uri.parse(formURL))
//        or
//        pdfView.fromFile(File)
//        or
//        pdfView.fromBytes(byte[])
//        or
//        pdfView.fromStream(InputStream) // stream is written to bytearray - native code cannot use Java Streams
//        or
//        pdfView.fromSource(DocumentSource)
//        or
//        pdfView.fromAsset(String)
                .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                // allows to draw something on the current page, usually visible in the middle of the screen
                .onDraw(this)
                // allows to draw something on all pages, separately for every page. Called only for visible pages
//                .onDrawAll(this)
                .onLoad(this) // called after document is loaded and starts to be rendered
                .onPageChange(this)
                .onPageScroll(this)
                .onError(this)
                .onRender(this) // called after document is rendered for the first time
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
//                .spacing(0)
                .load();


        return myFragmentView;
    }

    @Override
    public void loadComplete(int nbPages) {

    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }

    @Override
    public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {

    }

    @Override
    public void onPageScrolled(int page, float positionOffset) {

    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void onInitiallyRendered(int nbPages, float pageWidth, float pageHeight) {

    }
}
