package hnweb.com.ekasst.fragment.Customer;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.adapter.BookStoreAdapter;
import hnweb.com.ekasst.adapter.SearchBookAdapter;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.models.BookStore;
import hnweb.com.ekasst.models.Category;
import hnweb.com.ekasst.models.SearchBook;
import hnweb.com.ekasst.utils.AppAPI;

/**
 * Created by neha on 6/28/2017.
 */

public class SearchFragment extends Fragment {

    RecyclerView bookStoreRV;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    SharedPreferences sharedPreferences;
    ArrayList<SearchBook> bookStoreArrayList = new ArrayList<SearchBook>();
    ArrayList<BookStore> allBookArrayList = new ArrayList<BookStore>();
    ListView timeLV;
    ArrayList<Category> categoryArrayList = new ArrayList<Category>();
    EditText bookNameET;
    EditText categoryET;
    Button searchBookBTN;

    boolean visible = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (visible) {
            if (isVisibleToUser) {
                doGetAllBooksList();
            }
        }
    }


    public SearchFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_search, container, false);
        visible = true;
        ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.VISIBLE);
//        ((HomeActivity) getActivity()).bookStoreTV.setBackgroundResource(0);
//        ((HomeActivity) getActivity()).searchLL.setBackgroundResource(R.drawable.text_view_border);
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        bookStoreRV = (RecyclerView) myFragmentView.findViewById(R.id.booksRV);
        bookStoreRV.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        bookStoreRV.setLayoutManager(layoutManager);

        bookNameET = (EditText) myFragmentView.findViewById(R.id.bookNameET);
//        categoryET = (EditText) myFragmentView.findViewById(R.id.categoryET);
        searchBookBTN = (Button) myFragmentView.findViewById(R.id.searchBookBTN);

//        doGetCategoryList();
//        doGetBooksList();
        doGetAllBooksList();

        searchBookBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(bookNameET.getText().toString().trim())) {
                    doGetAllBooksList();
                    //TextUtils.isEmpty(categoryET.getText().toString().trim())
//                    bookNameET.setError("Please enter book or author name.");
//                    bookNameET.requestFocus();
//                    categoryET.setError("Please enter category.");
                }
//                else if (TextUtils.isEmpty(bookNameET.getText().toString().trim())) {
//                    bookNameET.setError("Please enter book or author name.");
//                    bookNameET.requestFocus();
//                }
//                else if (TextUtils.isEmpty(categoryET.getText().toString().trim())){
//                    categoryET.setError("Please enter category.");
//                    categoryET.requestFocus();
//                }
                else {
                    doGetBooksList();
                }

            }
        });

//        categoryET.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    selectCategory();
//                }
//
//
//                return false;
//            }
//        });

        return myFragmentView;
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {

                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getSearchBook")) {
                            JSONArray jarr = jobj.getJSONArray("parameters");
                            allBookArrayList.clear();
                            bookStoreArrayList.clear();
                            for (int i = 0; i < jarr.length(); i++) {
                                SearchBook bs = new SearchBook();
                                bs.setBook_id(jarr.getJSONObject(i).getString("book_id"));
                                bs.setBook_photo(jarr.getJSONObject(i).getString("book_photo"));
                                bs.setBook_title(jarr.getJSONObject(i).getString("book_title"));
                                bs.setBook_price(jarr.getJSONObject(i).getString("book_price"));
                                bookStoreArrayList.add(bs);
                            }

                            bookStoreRV.setAdapter(new SearchBookAdapter(getActivity(), bookStoreArrayList));

                        } else if (request_tag.equalsIgnoreCase("getBookCategory")) {
                            JSONArray jarr = jobj.getJSONArray("parameters");
                            allBookArrayList.clear();
                            bookStoreArrayList.clear();
                            for (int i = 0; i < jarr.length(); i++) {
                                Category bs = new Category();
                                bs.setCat_id(jarr.getJSONObject(i).getString("cat_id"));
                                bs.setCat_name(jarr.getJSONObject(i).getString("cat_name"));
                                bs.setCreated_by(jarr.getJSONObject(i).getString("created_by"));
                                bs.setCreated_on(jarr.getJSONObject(i).getString("created_on"));
                                categoryArrayList.add(bs);
                            }
                        } else if (request_tag.equalsIgnoreCase("getBookStore")) {
                            JSONArray jarr = jobj.getJSONArray("data");
                            bookStoreArrayList.clear();
                            allBookArrayList.clear();
                            for (int i = 0; i < jarr.length(); i++) {
                                BookStore bs = new BookStore();
                                bs.setBook_id(jarr.getJSONObject(i).getString("book_id"));
                                bs.setBook_photo(jarr.getJSONObject(i).getString("book_photo"));
                                bs.setBook_title(jarr.getJSONObject(i).getString("book_title"));
                                bs.setBook_price(jarr.getJSONObject(i).getString("book_price"));
                                allBookArrayList.add(bs);
                            }

                            bookStoreRV.setAdapter(new BookStoreAdapter(getActivity(), allBookArrayList, "HOME"));

                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {

            }
        };
    }

    public void doGetBooksList() {
        Map<String, String> params = new HashMap<>();
        params.put("search_parameter", bookNameET.getText().toString().trim());
        params.put("book_categories", "All");
        mVolleyService.postDataVolley("POSTCALL", AppAPI.SEARCH_A_BOOK, params, "getSearchBook");
    }

    public void doGetAllBooksList() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_BOOK_STORE, params, "getBookStore");
    }

    public void selectCategory() {

        final String[] categoryArr = new String[categoryArrayList.size()];

        for (int i = 0; i < categoryArrayList.size(); i++) {
            categoryArr[i] = categoryArrayList.get(i).getCat_name();
        }


        final Dialog settingsDialog = new Dialog(getActivity());
        settingsDialog.getWindow().setTitle("Select Category");
        settingsDialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.content_plans
                , null));
        settingsDialog.setCancelable(true);
        timeLV = (ListView) settingsDialog.findViewById(R.id.listView);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, categoryArr);
        timeLV.setAdapter(itemsAdapter);
        timeLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                settingsDialog.dismiss();

                categoryET.setError(null);
                categoryET.setText(categoryArr[i]);
            }
        });


        settingsDialog.show();
    }

    public void doGetCategoryList() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_CATEGORY_LIST, params, "getBookCategory");
    }
}
