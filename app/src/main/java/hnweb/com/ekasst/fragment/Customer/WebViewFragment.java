package hnweb.com.ekasst.fragment.Customer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import hnweb.com.ekasst.R;


/**
 * Created by neha on 6/2/2017.
 */

public class WebViewFragment extends Fragment {

    WebView myWebView;
    String formURL, title;
    int PIC_WIDTH;
    Toolbar toolbar;

    public WebViewFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_web_view, container, false);

        formURL = getArguments().getString("BOOKID");
//        title = getArguments().getString("Title");

//        ((HomeActivity) getContext()).toolTitle.setText(title);
        myWebView = (WebView) myFragmentView.findViewById(R.id.webview);

        myWebView.loadUrl("https://docs.google.com/viewerng/viewer?url=" + formURL);
//myWebView.loadUrl("http://www.residentialenergydynamics.com/REDCalcFree/Tools/ParallelPathEquivalentRValue");

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setUseWideViewPort(true);
        webSettings.getUseWideViewPort();
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        myWebView.setInitialScale(85);
        myWebView.setWebViewClient(new WebViewClient());

        //Make sure No cookies are created
        CookieManager.getInstance().setAcceptCookie(false);

//Make sure no caching is done
        myWebView.getSettings().setCacheMode(webSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setAppCacheEnabled(false);
        myWebView.clearHistory();
        myWebView.clearCache(true);


//Make sure no autofill for Forms/ user-name password happens for the app
        myWebView.clearFormData();
        myWebView.getSettings().setDomStorageEnabled(true);

        myWebView.getSettings().setSavePassword(false);
        myWebView.getSettings().setSaveFormData(false);


        return myFragmentView;
    }

    private class MyWebViewClient extends WebViewClient {

        @Override

        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            //http://www.residentialenergydynamics.com/Portals/0/REDCalc/Free/REDCalcFree.html?tool=ParallelPathRValue&_=2016-07-06_01:30
            if (Uri.parse(url).getHost().equals(formURL)) {

                return false;

            }


            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

            startActivity(intent);

            return true;
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
//            myWebView.goBack(); // Go to previous page
//            return true;
//        }
//        // Use this as else part
//        return super.onKeyDown(keyCode, event);
//    }

    private int getScale() {
        PIC_WIDTH = myWebView.getRight() - myWebView.getLeft();
        Point p = new Point();
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getSize(p);
        int width = p.x;
        Double val = new Double(width) / new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }

    @JavascriptInterface
    public void resize(final float height) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myWebView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, (int) (height * getResources().getDisplayMetrics().density)));
            }
        });
    }
}
