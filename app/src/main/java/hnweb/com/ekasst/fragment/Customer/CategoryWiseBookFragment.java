package hnweb.com.ekasst.fragment.Customer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.adapter.BookStoreAdapter;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.models.BookStore;
import hnweb.com.ekasst.utils.AppAPI;

/**
 * Created by neha on 7/1/2017.
 */

public class CategoryWiseBookFragment extends Fragment {

    RecyclerView bookStoreRV;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    SharedPreferences sharedPreferences;
    ArrayList<BookStore> bookStoreArrayList = new ArrayList<BookStore>();
    String cat_id;

    public CategoryWiseBookFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_categorywise_book, container, false);

        ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("");
        cat_id = getArguments().getString("BOOKID");
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        bookStoreRV = (RecyclerView) myFragmentView.findViewById(R.id.booksRV);
        bookStoreRV.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        bookStoreRV.setLayoutManager(layoutManager);

        doGetBooksList();


        return myFragmentView;
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {

                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getCategoryBookStore")) {
                            JSONArray jarr = jobj.getJSONArray("parameters");
                            if (jarr.length() == 0) {
                                Toast.makeText(getActivity(), "No books Available.", Toast.LENGTH_SHORT).show();
                            } else {
                                bookStoreArrayList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    BookStore bs = new BookStore();
                                    bs.setBook_id(jarr.getJSONObject(i).getString("book_id"));
                                    bs.setBook_photo(jarr.getJSONObject(i).getString("book_photo"));
                                    bs.setBook_title(jarr.getJSONObject(i).getString("book_title"));
                                    bs.setBook_price(jarr.getJSONObject(i).getString("book_price"));
                                    bookStoreArrayList.add(bs);
                                }

                                bookStoreRV.setAdapter(new BookStoreAdapter(getActivity(), bookStoreArrayList, "HOME"));
                            }
//

                        }
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {

            }
        };
    }

    public void doGetBooksList() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        params.put("category_id", cat_id);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.CATEGORY_WISE_BOOKS, params, "getCategoryBookStore");
    }

}
