package hnweb.com.ekasst.fragment.Customer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.MultipartRequest.MultiPart_Key_Value_Model;
import hnweb.com.ekasst.MultipartRequest.MultipartFileUploaderAsync;
import hnweb.com.ekasst.MultipartRequest.OnEventListener;
import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.utils.AppAPI;
import hnweb.com.ekasst.utils.CircleTransform;
import hnweb.com.ekasst.utils.RealPathUtil;

/**
 * Created by neha on 6/26/2017.
 */

public class MyProfileFragment extends Fragment {
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public EditText nameET, emailET, phoneET, passET, conPassET;
    CheckBox passCB;
    Button sendBTN;
    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private Uri selectedImageUri;
    LinearLayout passLL;
    private String realPath = "";
    private final CharSequence[] items = {"Take Photo", "From Gallery"};
    ImageView profileIV;
    ArrayList<String> imagesList = new ArrayList<String>();
    ArrayList<MultiPart_Key_Value_Model> mult_list;
    ProgressDialog progressDialog;
    File destination;

    public MyProfileFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("My Profile");
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        editor = sharedPreferences.edit();
        progressDialog = new ProgressDialog(getActivity());
        initView(myFragmentView);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        doViewProfile();
        return myFragmentView;
    }

    public void initView(View v) {

        passCB = (CheckBox) v.findViewById(R.id.passCB);
        nameET = (EditText) v.findViewById(R.id.nameET);
        emailET = (EditText) v.findViewById(R.id.emailET);
//        phoneET = (EditText) v.findViewById(R.id.phoneET);
        passLL = (LinearLayout) v.findViewById(R.id.passLL);
        passET = (EditText) v.findViewById(R.id.passET);
        conPassET = (EditText) v.findViewById(R.id.conPassET);
        sendBTN = (Button) v.findViewById(R.id.sendBTN);
        profileIV = (ImageView) v.findViewById(R.id.profileIV);
        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(nameET.getText().toString().trim())) {
//                        && TextUtils.isEmpty(emailET.getText().toString().trim())) {
//                        && TextUtils.isEmpty(phoneET.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter Name.", Toast.LENGTH_SHORT).show();
                }
//                } else if (TextUtils.isEmpty(nameET.getText().toString().trim())) {
//
//                }
//                else if (TextUtils.isEmpty(emailET.getText().toString().trim())) {
//
//                }
//                else if (TextUtils.isEmpty(phoneET.getText().toString().trim())) {
//
//                }
                else {
                    if (passCB.isChecked()) {
                        if (TextUtils.isEmpty(passET.getText().toString().trim()) && TextUtils.isEmpty(conPassET.getText().toString().trim())) {
                            passET.setError("Please enter password.");
                            passET.requestFocus();
                            conPassET.setError("Please enter confirm password.");
                        } else if (TextUtils.isEmpty(passET.getText().toString().trim())|| passET.getText().toString().trim().length() < 7) {
                            passET.setError("Please enter password of minimum 7 characters.");
                            passET.requestFocus();
                        } else if (!passET.getText().toString().trim().equalsIgnoreCase(conPassET.getText().toString().trim())) {
                            conPassET.setError("Password and confirm password not same.");
                        } else {
                            doEditProfile();
                        }

                    } else {
                        doEditProfile();
                    }

                }
            }
        });

        passCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    passET.setEnabled(true);
                    conPassET.setEnabled(true);
                    passLL.setVisibility(View.VISIBLE);
                } else {
                    passET.setEnabled(false);
                    conPassET.setEnabled(false);
                    passLL.setVisibility(View.GONE);
                }
            }
        });

        profileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooserDialog();
            }
        });

        nameET.setText(sharedPreferences.getString("NAME", ""));
        emailET.setText(sharedPreferences.getString("EMAIL_ID", ""));
//        phoneET.setText(sharedPreferences.getString("PHONE", ""));
    }


    public void doViewProfile() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_PROFILE, params, "getProfile");
    }

    public void doEditProfilePicture(String realPath) {

        mult_list = new ArrayList<MultiPart_Key_Value_Model>();

        MultiPart_Key_Value_Model OneObject = new MultiPart_Key_Value_Model();

        Map<String, String> fileParams = new HashMap<>();
        fileParams.put("profile_photo", realPath);

        Map<String, String> Stringparams = new HashMap<>();
        Stringparams.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));


        String requestURL = AppAPI.UPDATE_PROFILE_PICTURE;
        OneObject.setUrl(requestURL);
        OneObject.setFileparams(fileParams);
        OneObject.setStringparams(Stringparams);

        MultipartFileUploaderAsync someTask = new MultipartFileUploaderAsync(getActivity().getApplicationContext(), OneObject, new OnEventListener<String>() {

            @Override
            public void onSuccess(String object) {

//                Toast.makeText(getApplicationContext(), "" + object, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jobj = new JSONObject(object);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (message_code == 1) {
//                        imagesList.clear();
//                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();
//                        notify();
                    } else {
                        progressDialog.dismiss();
                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        someTask.execute();
        return;


//        Map<String, String> params = new HashMap<>();
//        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
//        params.put("profile_photo", realPath);
//        mVolleyService.postDataVolley("POSTCALL", AppAPI.UPDATE_PROFILE_PICTURE, params, "updateProfilePicture");
    }

    public void doEditProfile() {
        String password;
        if (TextUtils.isEmpty(passET.getText().toString().trim())) {
            password = String.valueOf(sharedPreferences.getInt("PASSWORD", 0));
        } else {
            password = passET.getText().toString().trim();
        }

        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        params.put("name", nameET.getText().toString().trim());
        params.put("email_address", emailET.getText().toString().trim());
        params.put("phone_number", sharedPreferences.getString("PHONE", ""));
        params.put("password", password);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.EDIT_PROFILE, params, "editProfile");
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getProfile")) {
                            String user_id = jobj.getString("user_id");
//                            String user_type = jobj.getString("user_type");
                            String name = jobj.getString("name");
                            String email_address = jobj.getString("email_address");
                            String phone_number = jobj.getString("phone_number");
                            String profilePhoto = jobj.getString("profile_photo");
                            String password = jobj.getString("password");

                            saveToSharedPref(user_id, "", name, email_address, phone_number, profilePhoto, password);

                            nameET.setText(name);
                            emailET.setText(email_address);
//                            phoneET.setText(phone_number);
                            if (!TextUtils.isEmpty(profilePhoto)) {
                                Glide.with(getActivity())
                                        .load(profilePhoto)
                                        .transform(new CircleTransform(getActivity()))
                                        .into(profileIV);
                                Glide.with(getActivity())
                                        .load(profilePhoto)
                                        .transform(new CircleTransform(getActivity()))
                                        .into(((HomeActivity) getActivity()).proPicIV);
                            }

                        } else if (request_tag.equalsIgnoreCase("editProfile")) {
                            passCB.setChecked(false);
                            passET.setText("");
                            conPassET.setText("");
                            doViewProfile();

                        }

//                        Intent intent = new Intent(getActivity(), HomeActivity.class);
//                        startActivity(intent);
//                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void saveToSharedPref(String user_id, String user_type, String name, String email_address, String phone_number, String profilePhoto, String password) {
        editor.putInt("USER_ID", Integer.parseInt(user_id));
        editor.putString("USER_TYPE", user_type);
        editor.putString("NAME", name);
        editor.putString("PHONE", phone_number);
        editor.putString("EMAIL_ID", email_address);
        editor.putString("PROFILE_PHOTO", profilePhoto);
        editor.putString("PASSWORD", password);
        Log.e("PASSWORD", password);

        editor.commit();
    }


    private void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(getActivity(), "Permission to use Camera", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            takePictureIntent();
//            initCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureIntent();
//                initCameraIntent();
            } else {
                Toast.makeText(getActivity(), "Permission denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }


    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
//        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".my.package.name.provider", file));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                realPath = cameraPath();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());
                }
                Log.d("REAL PATH", "real path: " + realPath);
            }
            Glide.with(getActivity()).load(new File(realPath)).override(150, 150).transform(new CircleTransform(getActivity())).into(profileIV);

            progressDialog.show();
            progressDialog.setMessage("Uploading...");
            progressDialog.setCancelable(false);
            doEditProfilePicture(realPath);
//            mBitmap = ImageUtils.getScaledImage(selectedImageUri, getActivity());
//            setImageBitmap(mBitmap);
        }

    }

//    public void doSendProgressReport() {
//
//
//        mult_list = new ArrayList<MultiPart_Key_Value_Model>();
//
//        MultiPart_Key_Value_Model OneObject = new MultiPart_Key_Value_Model();
//
//
//        Map<String, String> fileParams = new HashMap<>();
////        fileParams.put("images[" + i + "]", realPath);
////        for (int i = 0; i < imagesList.size(); i++) {
////            fileParams.put("images[" + i + "]", imagesList.get(i).toString());
////        }
//
//        Map<String, String> Stringparams = new HashMap<>();
//        Stringparams.put("Email", sharedPreferences.getString("Email", ""));
//
//
//        String requestURL = AppAPI.sendProgressReport;
//        OneObject.setUrl(requestURL);
//        OneObject.setFileparams(fileParams);
//        OneObject.setStringparams(Stringparams);
//
//        MultipartFileUploaderAsync someTask = new MultipartFileUploaderAsync(getActivity().getApplicationContext(), OneObject, new OnEventListener<String>() {
//
//            @Override
//            public void onSuccess(String object) {
//
////                Toast.makeText(getApplicationContext(), "" + object, Toast.LENGTH_SHORT).show();
//                try {
//                    JSONObject jobj = new JSONObject(object);
//                    int message_code = jobj.getInt("message_code");
//                    String message = jobj.getString("message");
//                    if (message_code == 1) {
//                        imagesList.clear();
//                        adapter.notifyDataSetChanged();
//                        progressDialog.dismiss();
////                        notify();
//                    } else {
//                        progressDialog.dismiss();
//                    }
//
//                    Toast.makeText(ProgressReportUploadActivity.this, message, Toast.LENGTH_SHORT).show();
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//
//            }
//        });
//        someTask.execute();
//        return;
//
//    }

    public void takePictureIntent() {
        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
        destination = new File(Environment.getExternalStorageDirectory(), name + ".png");


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));

        //arshad
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", destination));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public String cameraPath() {
        try {
            FileInputStream in = new FileInputStream(destination);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 10;
            String imagePath = destination.getAbsolutePath();
            Log.i("Path", imagePath);
//            bmp = BitmapFactory.decodeStream(in, null, options);

//            uploadIV.setImageBitmap(bmp);
            return imagePath;

//            picture.setImageBitmap(bmp);
//            imageVideoUpload(imagePath, hunt_id, hunt_item_id, user_id);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

}
