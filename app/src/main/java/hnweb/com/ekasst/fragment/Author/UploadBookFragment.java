package hnweb.com.ekasst.fragment.Author;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.MultipartRequest.MultiPart_Key_Value_Model;
import hnweb.com.ekasst.MultipartRequest.MultipartFileUploaderAsync;
import hnweb.com.ekasst.MultipartRequest.OnEventListener;
import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.fragment.Customer.MyBookShelfFragment;
import hnweb.com.ekasst.models.Category;
import hnweb.com.ekasst.utils.AppAPI;
import hnweb.com.ekasst.utils.FilePath;
import hnweb.com.ekasst.utils.RealPathUtil;

/**
 * Created by neha on 7/11/2017.
 */

public class UploadBookFragment extends Fragment {
    EditText bookNameET, authorNameET, bookPriceET, descriptionET, bookLanguageET, bookCategoryET;
    Button attachBTN, uploadBookBTN;
    final int ACTIVITY_CHOOSE_FILE = 2;
    TextView pdfPathTV;
    File destination;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";

    SharedPreferences sharedPreferences;
    LinearLayout addBookLL;
    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private Uri selectedImageUri;
    private String realPath = "";
    private final CharSequence[] items = {"Take Photo", "From Gallery"};
    ImageView bookImageIV;
    ArrayList<MultiPart_Key_Value_Model> mult_list;
    ProgressDialog progressDialog;
    Uri fileUri;
    String filePath;
    Button bookCoverBTN;
    TextView coverPathTV;
    ArrayList<Category> bookStoreArrayList = new ArrayList<Category>();

    public UploadBookFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_upload_book, container, false);

        ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.GONE);
        ((HomeActivity) getActivity()).toolbar.setTitle("Upload Book");
        progressDialog = new ProgressDialog(getActivity());
        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        init(myFragmentView);
        doGetBooksCategooryList();

        return myFragmentView;
    }

    public void init(View v) {

        bookNameET = (EditText) v.findViewById(R.id.bookNameET);
//        authorNameET = (EditText) v.findViewById(R.id.authorNameET);
        bookPriceET = (EditText) v.findViewById(R.id.bookPriceET);
        descriptionET = (EditText) v.findViewById(R.id.descriptionET);
        bookLanguageET = (EditText) v.findViewById(R.id.bookLanguageET);
        bookCategoryET = (EditText) v.findViewById(R.id.bookCategoryET);
//        addBookLL = (LinearLayout) v.findViewById(R.id.addBookLL);
        bookCoverBTN = (Button) v.findViewById(R.id.bookCoverBTN);
        coverPathTV = (TextView) v.findViewById(R.id.coverPathTV);
        attachBTN = (Button) v.findViewById(R.id.attachBTN);
//        bookImageIV = (ImageView) v.findViewById(R.id.bookImageIV);
        pdfPathTV = (TextView) v.findViewById(R.id.pdfPathTV);
        attachBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("application/pdf");
                startActivityForResult(Intent.createChooser(chooseFile, "Select Pdf"), ACTIVITY_CHOOSE_FILE);
            }
        });

        uploadBookBTN = (Button) v.findViewById(R.id.uploadBookBTN);
        uploadBookBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValid();
            }
        });

        bookCoverBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooserDialog();
            }
        });
        bookCategoryET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    if (bookStoreArrayList.size() > 0) {
                        categoriesDialog(bookStoreArrayList);
                    }

                }

                return true;
            }
        });

//        bookCategoryET.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

    }

    public void checkValid() {
////        String path = FilePath.getPath(getActivity(), filePath);
//        String path = filePath.getPath();
////        path = (new File(URI.create(filePath.toString()))).getAbsolutePath();
//        Log.e("PATH", path);
//        pdfPathTV.setVisibility(View.VISIBLE);
//        pdfPathTV.setText(path);
        if (TextUtils.isEmpty(bookNameET.getText().toString().trim())

                && TextUtils.isEmpty(bookPriceET.getText().toString().trim())
                && TextUtils.isEmpty(descriptionET.getText().toString().trim())
                && TextUtils.isEmpty(bookLanguageET.getText().toString().trim())

                && TextUtils.isEmpty(pdfPathTV.getText().toString().trim())
                && TextUtils.isEmpty(realPath)
                && TextUtils.isEmpty(bookCategoryET.getText().toString().trim())) {
            //&& TextUtils.isEmpty(authorNameET.getText().toString().trim())
            //&& TextUtils.isEmpty(bookCategoryET.getText().toString().trim())
            bookNameET.setError("Please enter book name.");
            bookNameET.requestFocus();
//            authorNameET.setError("Please enter author name.");
            bookPriceET.setError("Please enter book price.");
            descriptionET.setError("Please enter book description.");
            bookLanguageET.setError("Please enter book language.");
            bookCategoryET.setError("Please enter book category.");
            Toast.makeText(getActivity(), "Please select book image and attach book pdf file.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(realPath)) {
            Toast.makeText(getActivity(), "Please select book image.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(bookNameET.getText().toString().trim())) {
            bookNameET.setError("Please enter book name.");
            bookNameET.requestFocus();
        }
//        else if (TextUtils.isEmpty(authorNameET.getText().toString().trim())) {
//            authorNameET.setError("Please enter author name.");
//            authorNameET.requestFocus();
//        }
        else if (TextUtils.isEmpty(bookPriceET.getText().toString().trim())) {
            bookPriceET.setError("Please enter book price.");
            bookPriceET.requestFocus();
        } else if (TextUtils.isEmpty(descriptionET.getText().toString().trim())) {
            descriptionET.requestFocus();
            descriptionET.setError("Please enter book description.");
        } else if (TextUtils.isEmpty(bookLanguageET.getText().toString().trim())) {
            bookLanguageET.setError("Please enter book language.");
            bookLanguageET.requestFocus();
        } else if (TextUtils.isEmpty(bookCategoryET.getText().toString().trim())) {
            bookCategoryET.setError("Please enter book category.");
            bookCategoryET.requestFocus();
        } else if (TextUtils.isEmpty(pdfPathTV.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Please attach book pdf file.", Toast.LENGTH_SHORT).show();
        } else {

            if (filePath == null) {
                Toast.makeText(getActivity(), "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
            } else {
                progressDialog.show();
                progressDialog.setMessage("Uploading...");
                progressDialog.setCancelable(false);

                doUploadBooks(bookNameET.getText().toString().trim(), "author",
                        bookPriceET.getText().toString().trim(), descriptionET.getText().toString().trim(),
                        bookLanguageET.getText().toString().trim(), pdfPathTV.getText().toString().trim(), realPath,
                        bookCategoryET.getText().toString().trim());
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ACTIVITY_CHOOSE_FILE) {
            // Make sure the request was successful
            if (resultCode == getActivity().RESULT_OK) {


                // get the file uri
                fileUri = data.getData();
                filePath = FilePath.getPath(getActivity(), fileUri);
                if (filePath == null) {
                    Toast.makeText(getActivity(), "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
                } else {
                    pdfPathTV.setText(filePath);
                    pdfPathTV.setVisibility(View.VISIBLE);
                }

            }
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                realPath = cameraPath();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());
                }
                Log.d("REAL PATH", "real path: " + realPath);
            }
            coverPathTV.setVisibility(View.VISIBLE);
            coverPathTV.setText(realPath);
//            Glide.with(getActivity()).load(new File(realPath)).into(bookImageIV);

        }

    }

//    private String validateFile(Uri uri) {
//        String mssg = null;
//        String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
//        File file = new File(uri.getPath());
//        // is the file readable?
////        mssg = uri.getPath();
//        if (!file.canRead()) {
//            mssg = uri.getPath();
////            mssg = "File selected1: ".concat(uri.getPath()).concat("\nPlease, select a PDF file from your SD card");
//        }
//        // simple extension check
//        else if (!extension.toLowerCase().equals("pdf")) {
//            mssg = uri.getPath();
////            mssg = "File selected2: ".concat(uri.getPath()).concat("\nPlease, select a PDF file from your SD card");
//        }
//        return mssg;
//    }


    public void doUploadBooks(String bookName, String authorName, String bookPrice,
                              String description, String bookLanguage, String pdfPath, String bookImage, String bookCategory) {

//        try {
//            String uploadId = UUID.randomUUID().toString();
//
//            //Creating a multi part request
//            new MultipartUploadRequest(getActivity(), uploadId, AppAPI.UPLOAD_BOOK)
//                    .addFileToUpload(pdfPath, "book_pdf_link") //Adding file
//                    .addFileToUpload(bookImage, "book_photo")
//                    .addParameter("author_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)))//Adding text parameter to the request
//                    .addParameter("book_title", bookName)
//                    .addParameter("book_categories", bookCategory)
//                    .addParameter("book_description", description)
//                    .addParameter("book_price", bookPrice)
//                    .setNotificationConfig(new UploadNotificationConfig())
//                    .setMaxRetries(2)
//                    .startUpload(); //Starting the upload
//
//        } catch (Exception exc) {
//            Toast.makeText(getActivity(), exc.getMessage(), Toast.LENGTH_SHORT).show();
//        }


        mult_list = new ArrayList<MultiPart_Key_Value_Model>();

        MultiPart_Key_Value_Model OneObject = new MultiPart_Key_Value_Model();

        Map<String, String> fileParams = new HashMap<>();
        fileParams.put("book_photo", bookImage);
        fileParams.put("book_pdf_link", pdfPath);

        Map<String, String> Stringparams = new HashMap<>();
        Stringparams.put("author_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        Stringparams.put("book_title", bookName);
        Stringparams.put("book_categories", bookCategory);
        Stringparams.put("book_description", description);
        Stringparams.put("book_price", bookPrice);
        Stringparams.put("book_language", bookLanguage);

        String requestURL = AppAPI.NEW_BOOK_UPLOAD;
        OneObject.setUrl(requestURL);
        OneObject.setFileparams(fileParams);
        OneObject.setStringparams(Stringparams);

        MultipartFileUploaderAsync someTask = new MultipartFileUploaderAsync(getActivity(), OneObject, new OnEventListener<String>() {

            @Override
            public void onSuccess(String object) {

//                Log.e("RESPONSE", object);
//                Toast.makeText(getActivity(), "" + object, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jobj = new JSONObject(object);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (message_code == 1) {
//                        imagesList.clear();
//                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                        MyBookShelfFragment myBookShelfFragment = new MyBookShelfFragment();
                        ((HomeActivity) getActivity()).replaceFragment(myBookShelfFragment);
//                        notify();
                    } else {
                        progressDialog.dismiss();
                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        someTask.execute();
        return;

//
//        Map<String, String> params = new HashMap<>();
//        params.put("author_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
//        params.put("book_title", bookName);
//        params.put("book_categories", authorName);
//        params.put("book_description", bookPrice);
//        params.put("book_price", description);
////        params.put("book_language", bookLanguage);
//        //params.put("book_category", bookCategory);
//        params.put("book_pdf_link", pdfPath);
//        params.put("book_photo", bookImage);
////        mVolleyService.postDataVolley("POSTCALL", AppAPI.UPLOAD_BOOK, params, "getUploadBook");

    }

    private void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(getActivity(), "Permission to use Camera", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            takePictureIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureIntent();
            } else {
                Toast.makeText(getActivity(), "Permission denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
//        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", file));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        return mediaFile;
    }

    public static String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public void takePictureIntent() {
        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
        destination = new File(Environment.getExternalStorageDirectory(), name + ".png");


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));

        //arshad
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", destination));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public String cameraPath() {
        try {
            FileInputStream in = new FileInputStream(destination);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 10;
            String imagePath = destination.getAbsolutePath();
            Log.i("Path", imagePath);
//            bmp = BitmapFactory.decodeStream(in, null, options);

//            uploadIV.setImageBitmap(bmp);
            return imagePath;

//            picture.setImageBitmap(bmp);
//            imageVideoUpload(imagePath, hunt_id, hunt_item_id, user_id);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void doGetBooksCategooryList() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_CATEGORY_LIST, params, "getBookCategory");
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {

                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getBookCategory")) {
                            JSONArray jarr = jobj.getJSONArray("parameters");
                            bookStoreArrayList.clear();
                            for (int i = 0; i < jarr.length(); i++) {
                                Category bs = new Category();
                                bs.setCat_id(jarr.getJSONObject(i).getString("cat_id"));
                                bs.setCat_name(jarr.getJSONObject(i).getString("cat_name"));
                                bs.setCreated_by(jarr.getJSONObject(i).getString("created_by"));
                                bs.setCreated_on(jarr.getJSONObject(i).getString("created_on"));
                                bookStoreArrayList.add(bs);
                            }

//                            bookStoreRV.setAdapter(new CategoryAdapter(getActivity(),bookStoreArrayList));

                        }
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {

            }
        };
    }


    public void categoriesDialog(final ArrayList<Category> bookStoreArrayList) {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < bookStoreArrayList.size(); i++) {
            list.add(bookStoreArrayList.get(i).getCat_name());
        }

        final Dialog settingsDialog = new Dialog(getActivity());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setCancelable(false);
        settingsDialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.category_list_item
                , null));
//        settingsDialog.setCancelable(true);

        ListView maritalStatusLV = (ListView) settingsDialog.findViewById(R.id.maritalStatusLV);
        maritalStatusLV.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list));
//        final Button upgradeBTN = (Button) settingsDialog.findViewById(R.id.upgradeBTN);
//        Button remindBTN = (Button) settingsDialog.findViewById(R.id.remindBTN);
//
//        upgradeBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                underDevelopment(context);
//            }
//        });
//        remindBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                settingsDialog.dismiss();
//            }
//        });
        maritalStatusLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                bookCategoryET.setText(bookStoreArrayList.get(i).getCat_name());
                bookNameET.setError("");
                settingsDialog.dismiss();
            }
        });
        settingsDialog.show();
    }

}
