package hnweb.com.ekasst.fragment.Customer;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.activity.HomeActivity;
import hnweb.com.ekasst.adapter.BookDisplayAdapter;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.models.BookStore;
import hnweb.com.ekasst.utils.AppAPI;
import hnweb.com.ekasst.utils.DepthPageTransformer;

/**
 * Created by neha on 7/21/2017.
 */

public class BookDisplayFragment extends Fragment implements ViewPager.OnPageChangeListener {
    ImageView bookIV;
    TextView bookNameTV, authorTV, priceTV, descriptionTV, detailsTV;
    Button buyNowBTN;
    SharedPreferences sharedPreferences;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    String book_id;
    String formURL;
    ViewPager viewPager;
    public Button prveTV, nextTV;
    ArrayList<String> myList;

    ArrayList<BookStore> bookStoreArrayList = new ArrayList<BookStore>();


    public BookDisplayFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_display_book, container, false);

        book_id = getArguments().getString("BOOKID");

        sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(getActivity().getPackageName(), 0);
        viewPager = (ViewPager) layout.findViewById(R.id.viewpager);
        nextTV = (Button) layout.findViewById(R.id.nextTV);
        prveTV = (Button) layout.findViewById(R.id.prveTV);
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOnPageChangeListener(this);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());

        if (viewPager.getCurrentItem() == 0) {
            prveTV.setVisibility(View.GONE);
        } else if (viewPager.getCurrentItem() + 1 == myList.size()) {
            nextTV.setVisibility(View.GONE);
        }
//        else if (viewPager.getCurrentItem() == 1) {
//            prveTV.setVisibility(View.GONE);
//        } else if (viewPager.getCurrentItem() == myList.size()) {
//            nextTV.setVisibility(View.GONE);
//        }
        prveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), viewPager.getCurrentItem() + "", Toast.LENGTH_SHORT).show();

                if (viewPager.getCurrentItem() > 0) {
                    nextTV.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
                } else {
                    prveTV.setVisibility(View.GONE);
                }
//                else {
//                    prveTV.setVisibility(View.GONE);
//                }
            }

        });
        nextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() + 1 < myList.size()) {
                    prveTV.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);//getItem(-1) for previous
                } else {

                    nextTV.setVisibility(View.GONE);
                }
            }
        });
//        collection.addView(layout);

        doGetBookDetails(book_id);
        return layout;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position > 0) {
            prveTV.setVisibility(View.VISIBLE);

        } else if (position + 1 < myList.size()) {
            nextTV.setVisibility(View.VISIBLE);
        }

//        if (viewPager.getCurrentItem() == 0) {
//            prveTV.setVisibility(View.GONE);
//        } else if (viewPager.getCurrentItem() + 1 == myList.size()) {
//            nextTV.setVisibility(View.GONE);
//        }else {
//            prveTV.setVisibility(View.VISIBLE);
//            nextTV.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getBookDisplay")) {
                            JSONArray jarr = jobj.getJSONArray("parameters");
                            if (jarr.length() == 0) {
                                Toast.makeText(getActivity(), "This book not Available in image format.", Toast.LENGTH_SHORT).show();
                            } else {

                                bookStoreArrayList.clear();
                                for (int i = 0; i < jarr.length(); i++) {
                                    BookStore bs = new BookStore();
                                    bs.setBook_id(jarr.getJSONObject(i).getString("book_id"));
                                    bs.setBook_photo(jarr.getJSONObject(i).getString("book_photo"));
                                    bs.setBook_title(jarr.getJSONObject(i).getString("book_title"));
                                    bs.setBook_price(jarr.getJSONObject(i).getString("book_price"));
                                    bs.setBook_pdf_link(jarr.getJSONObject(i).getString("images"));
                                    bookStoreArrayList.add(bs);
                                }

                                ((HomeActivity) getActivity()).titleToolLL.setVisibility(View.GONE);
                                ((HomeActivity) getActivity()).toolbar.setTitle(jarr.getJSONObject(0).getString("book_title"));
                                String s = jarr.getJSONObject(0).getString("images");
                                myList = new ArrayList<String>(Arrays.asList(s.split(",")));

                                viewPager.setAdapter(new BookDisplayAdapter(getActivity(), myList, "HOME"));
                            }
                        }


                    }

//                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void doGetBookDetails(String book_id) {
        Map<String, String> params = new HashMap<>();
        params.put("book_id", book_id);
        params.put("user_id", String.valueOf(sharedPreferences.getInt("USER_ID", 0)));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.BOOK_DISPLAY, params, "getBookDisplay");
    }

}
