package hnweb.com.ekasst.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.fragment.Customer.MyBookShelfFragment;
import hnweb.com.ekasst.fragment.Customer.SearchFragment;
import hnweb.com.ekasst.fragment.Customer.TheBookStoreFragment;
import hnweb.com.ekasst.utils.CircleTransform;
import hnweb.com.ekasst.utils.Drawer;

public class HomeActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public LinearLayout titleToolLL;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    RelativeLayout settingDrawerLL;
    public TextView bookStoreTV, categoryTV, searchTV;
    SharedPreferences sharedPreferences;
    public ImageView proPicIV;
    public TextView userNameTV;
    public LinearLayout searchLL;
    Button homeTV;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        titleToolLL = (LinearLayout) toolbar.findViewById(R.id.titleLL);
        bookStoreTV = (TextView) toolbar.findViewById(R.id.bookStoreTV);
//        categoryTV = (TextView) toolbar.findViewById(R.id.categoryTV);
        searchLL = (LinearLayout) toolbar.findViewById(R.id.searchLL);
        searchTV = (TextView) toolbar.findViewById(R.id.searchTV);
//        titleToolLL.setVisibility(View.VISIBLE);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(new CustomPagerAdapter(getSupportFragmentManager(), HomeActivity.this));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setTitle("");


        bookStoreTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });

        searchLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (RelativeLayout) findViewById(R.id.settings_drawer_layout);

        proPicIV = (ImageView) settingDrawerLL.findViewById(R.id.proPicIV);
        if (!TextUtils.isEmpty(sharedPreferences.getString("PROFILE_PHOTO", ""))) {
            Glide.with(this)
                    .load(sharedPreferences.getString("PROFILE_PHOTO", ""))
                    .override(80, 80)
                    .transform(new CircleTransform(HomeActivity.this))
                    .into(proPicIV);
        }

        userNameTV = (TextView) settingDrawerLL.findViewById(R.id.userNameTV);
        userNameTV.setText(sharedPreferences.getString("NAME", ""));
        homeTV = (Button) settingDrawerLL.findViewById(R.id.homeTV);
        if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("Member")) {

            homeTV.setText("The Book Store");
        } else {
            toolbar.setTitle("My BookShelf");
            homeTV.setText("Upload Book");

        }
//        homeTV.setText();
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("Member")) {
            mViewPager.setVisibility(View.VISIBLE);
        } else {
            mViewPager.setVisibility(View.GONE);
            titleToolLL.setVisibility(View.GONE);
            toolbar.setTitle("UPLOAD BOOK");
            MyBookShelfFragment mf = new MyBookShelfFragment();
            replaceFragment(mf);

        }

        bookStoreTV.setBackgroundResource(R.drawable.text_view_border);
        searchLL.setBackgroundResource(0);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    bookStoreTV.setBackgroundResource(R.drawable.text_view_border);
                    searchLL.setBackgroundResource(0);
                } else if (position == 1) {
                    bookStoreTV.setBackgroundResource(0);
                    searchLL.setBackgroundResource(R.drawable.text_view_border);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void onDrawerClick(View v) {

        drawerLayout.closeDrawers();
        Drawer.clicked(this, v.getId());
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    public void replaceFragmentWithParams(android.support.v4.app.Fragment fragment, String book_id) {
        Bundle bundle = new Bundle();
        bundle.putString("BOOKID", book_id);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public class CustomPagerAdapter extends FragmentPagerAdapter {

        protected Activity mContext;

        public CustomPagerAdapter(FragmentManager fm, Activity context) {
            super(fm);
            mContext = context;
        }


        @Override
        // This method returns the fragment associated with
        // the specified position.
        //
        // It is called when the Adapter needs a fragment
        // and it does not exists.
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new TheBookStoreFragment();
                case 1:
                    return new SearchFragment();
                default:
                    return new TheBookStoreFragment();
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

    }

}
