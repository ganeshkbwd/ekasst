package hnweb.com.ekasst.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.utils.AppAPI;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    EditText emailET, fullNameET, passET, phoneET, conPassET;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    String USER_TYPE = "Member";
    RadioButton customerRB, authorRB;
    RadioGroup myRadioGroup;
    int selected = -1;
    ImageButton passAlertIV;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        editor = prefs.edit();

        initView();
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, this);


    }

    public void initView() {
        fullNameET = (EditText) findViewById(R.id.fullNameET);
        emailET = (EditText) findViewById(R.id.emailET);
        passET = (EditText) findViewById(R.id.passET);
//        phoneET = (EditText) findViewById(R.id.phoneET);
        conPassET = (EditText) findViewById(R.id.conPassET);
        customerRB = (RadioButton) findViewById(R.id.customerRB);
        authorRB = (RadioButton) findViewById(R.id.authorRB);
        myRadioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);
        passAlertIV = (ImageButton) findViewById(R.id.passAlertIV);
        myRadioGroup.setOnCheckedChangeListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registerBTN:
                checkValid();
                break;
            case R.id.loginBTNl:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.passAlertIV:
//                passAlertIV.setError("Passwords are “Case Sensitive.");
                Toast.makeText(this, "Passwords are Case Sensitive.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void checkValid() {

        selected = myRadioGroup.getCheckedRadioButtonId();
        if (selected == -1) {
            Toast.makeText(this, "Please select type customer or author.", Toast.LENGTH_SHORT).show();
        } else {
            if (TextUtils.isEmpty(fullNameET.getText().toString().trim()) &&
                    TextUtils.isEmpty(emailET.getText().toString().trim()) &&
                    TextUtils.isEmpty(phoneET.getText().toString().trim()) &&

                    TextUtils.isEmpty(conPassET.getText().toString().trim())) {
//                TextUtils.isEmpty(passET.getText().toString().trim()) &&

                fullNameET.setError("Please enter full name.");
                fullNameET.requestFocus();
                emailET.setError("Please enter email address.");
//                phoneET.setError("Please enter phone number.");
                passET.setError("Please enter password.");
                conPassET.setError("Please enter confirm password.");

            } else if (TextUtils.isEmpty(fullNameET.getText().toString().trim())) {
                fullNameET.setError("Please enter full name.");
                fullNameET.requestFocus();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString().trim()).matches()) {
                emailET.setError("Please enter valid email address.");
                emailET.requestFocus();
            }
//            else if (!Patterns.PHONE.matcher(phoneET.getText().toString().trim()).matches()) {
//                phoneET.setError("Please enter phone number.");
//                phoneET.requestFocus();
//            }
            else if (TextUtils.isEmpty(passET.getText().toString().trim()) || passET.getText().toString().trim().length() < 7) {
                passET.setError("Please enter password of minimum 7 characters.");
                passET.requestFocus();
            } else if (!passET.getText().toString().trim().equals(conPassET.getText().toString().trim())) {
                conPassET.setError("Please enter password and confirm password not match.");
                conPassET.requestFocus();
            } else {
//                Toast.makeText(this, "Under Development .....", Toast.LENGTH_SHORT).show();

                doRegistration();
            }
        }


    }

    public void doRegistration() {
        Map<String, String> params = new HashMap<>();
        params.put("user_type", USER_TYPE);
        params.put("name", fullNameET.getText().toString().trim());
        params.put("email_address", emailET.getText().toString().trim());
//        params.put("phone_number", phoneET.getText().toString().trim());
        params.put("password", passET.getText().toString().trim());


        mVolleyService.postDataVolley("POSTCALL", AppAPI.REGISTER_URL, params, "register");
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    if (meg_code == 1) {
                        if (request_tag.equalsIgnoreCase("login")) {
                            String user_id = jobj.getString("user_id");
                            String user_type = jobj.getString("user_type");
                            String name = jobj.getString("name");
                            String email_address = jobj.getString("email_address");
                            String phone_number = jobj.getString("phone_number");
                            String profile_photo = jobj.getString("profile_photo");

                            saveToSharedPref(user_id, user_type, name, email_address, phone_number, profile_photo);

                            if (user_type.equalsIgnoreCase("Member")) {
                                Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (user_type.equalsIgnoreCase("Author")) {
                                Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
//                            Toast.makeText(LoginActivity.this, "Under Development....", Toast.LENGTH_SHORT).show();
                            }

                        } else if (request_tag.equalsIgnoreCase("register")) {
                            doLogin();
                        }

//                        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
//                        startActivity(intent);
//                        finish();
                    }
                    String message = jobj.getString("message");


                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(RegisterActivity.this, "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void doLogin() {
        Map<String, String> params = new HashMap<>();
        params.put("user_type", USER_TYPE);
        params.put("email_address", emailET.getText().toString().trim());
        params.put("password", passET.getText().toString().trim());

        mVolleyService.postDataVolley("POSTCALL", AppAPI.LOGIN_URL, params, "login");
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        if (i == R.id.customerRB) {
            USER_TYPE = "Member";
        } else if (i == R.id.authorRB) {
            USER_TYPE = "Author";
        }
    }

    private void saveToSharedPref(String user_id, String user_type, String name, String email_address, String phone_number, String profile_photo) {
        editor.putInt("USER_ID", Integer.parseInt(user_id));
        editor.putString("USER_TYPE", user_type);
        editor.putString("NAME", name);
        editor.putString("PHONE", phone_number);
        editor.putString("EMAIL_ID", email_address);
        editor.putString("PROFILE_PHOTO", profile_photo);

        editor.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
