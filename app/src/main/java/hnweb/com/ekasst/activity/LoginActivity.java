package hnweb.com.ekasst.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.utils.AppAPI;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    EditText emailET, passET;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    String USER_TYPE = "Member";
    RadioButton customerRB, authorRB;
    RadioGroup myRadioGroup;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    public int selected = -1;
    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    LinearLayout fbLoginLL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        editor = prefs.edit();
        initView();
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, this);
//        doFBFeed();
//        getFeeds();
    }

    public void initView() {
        emailET = (EditText) findViewById(R.id.emailET);
        passET = (EditText) findViewById(R.id.passET);
        myRadioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);
        myRadioGroup.setOnCheckedChangeListener(this);
        customerRB = (RadioButton) findViewById(R.id.customerRB);
        authorRB = (RadioButton) findViewById(R.id.authorRB);
        fbLoginLL = (LinearLayout) findViewById(R.id.fbLoginLL);
        fbLoginLL.setOnClickListener(this);

    }

    public void doLogin() {
        Map<String, String> params = new HashMap<>();
        params.put("user_type", USER_TYPE);
        params.put("email_address", emailET.getText().toString().trim());
        params.put("password", passET.getText().toString().trim());

        mVolleyService.postDataVolley("POSTCALL", AppAPI.LOGIN_URL, params, "login");
    }

    public void doFacebookLogin(String id, String name, String FEmail, String profilePicUrl) {
        Map<String, String> params = new HashMap<>();
        params.put("user_type", USER_TYPE);
        params.put("email_address", FEmail);
        params.put("full_name", name);
        params.put("profile_photo", profilePicUrl);
        params.put("facebook_id", id);

        mVolleyService.postDataVolley("POSTCALL", AppAPI.FACEBOOK_LOGIN, params, "fb_login");
    }

    public void doFBFeed() {
        String URL = "https://developers.facebook.com/tools/explorer/145634995501895/?method=GET&path=SCBEC%2Fposts%3Ffields%3Dattachments&version=v2.10" +
                "SCBEC/posts?fields=attachments";
        mVolleyService.getDataVolley("GETCALL", URL, "fb_feed");
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);

                if (request_tag.equalsIgnoreCase("fb_feed")) {
                    Log.e(TAG, "Volley JSON post ERROR" + response);
                } else {
                    try {
                        JSONObject jobj = new JSONObject(response);
                        int meg_code = jobj.getInt("message_code");
                        String message = jobj.getString("message");

                        if (meg_code == 1) {
                            int fb_login = 0;
                            if (request_tag.equalsIgnoreCase("fb_login")) {
                                fb_login = 1;

                            } else if (request_tag.equalsIgnoreCase("login")) {
                                fb_login = 0;

                            }

                            String user_id = jobj.getString("user_id");
                            String user_type = jobj.getString("user_type");
                            String name = jobj.getString("name");
                            String email_address = "";
                            if (jobj.has("email_address")) {
                                email_address = jobj.getString("email_address");
                            }
                            if (jobj.has("email")) {
                                email_address = jobj.getString("email");
                            }
                            String phone_number = "";
                            if (jobj.has("phone_number")) {
                                phone_number = jobj.getString("phone_number");
                            }

                            String profile_photo = jobj.getString("profile_photo");

                            saveToSharedPref(user_id, user_type, name, email_address, phone_number, profile_photo, fb_login);

                            if (user_type.equalsIgnoreCase("Member")) {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (user_type.equalsIgnoreCase("Author")) {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
//                            Toast.makeText(LoginActivity.this, "Under Development....", Toast.LENGTH_SHORT).show();
                            }

                        }

                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(LoginActivity.this, "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void saveToSharedPref(String user_id, String user_type, String name, String email_address, String phone_number, String profile_photo, int fb_login) {
        editor.putInt("USER_ID", Integer.parseInt(user_id));
        editor.putString("USER_TYPE", user_type);
        editor.putString("NAME", name);
        editor.putString("PHONE", phone_number);
        editor.putString("EMAIL_ID", email_address);
        editor.putString("PROFILE_PHOTO", profile_photo);
        editor.putInt("FB_LOGIN", fb_login);

        editor.commit();
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.registerBTN:
                intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.fbLoginLL:
                selected = myRadioGroup.getCheckedRadioButtonId();

// Gets a reference to our "selected" radio button
                RadioButton b = (RadioButton) findViewById(selected);
// Now you can get the text or whatever you want from the "selected" radio button
//        b.getText();
                if (selected == -1) {

                    Toast.makeText(this, "Please select type customer or author.", Toast.LENGTH_SHORT).show();

                } else {
                    fbadd();
                }
//                Toast.makeText(this, "Under Development...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.loginBTN:

                checkValid();

                break;
            case R.id.fpBTN:
                intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
//                finish();
                break;
        }
    }

    public void checkValid() {

//        RadioGroup g = (RadioGroup) findViewById(R.id.rBtnDigits);

// Returns an integer which represents the selected radio button's ID
        selected = myRadioGroup.getCheckedRadioButtonId();

// Gets a reference to our "selected" radio button
        RadioButton b = (RadioButton) findViewById(selected);

// Now you can get the text or whatever you want from the "selected" radio button
//        b.getText();

        if (selected == -1) {

            Toast.makeText(this, "Please select type customer or author.", Toast.LENGTH_SHORT).show();

        } else {
            if (TextUtils.isEmpty(emailET.getText().toString().trim())
                    && TextUtils.isEmpty(passET.getText().toString().trim())) {
                emailET.setError("Please enter email address.");
                emailET.requestFocus();
                passET.setError("Please enter password.");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString().trim()).matches()) {
                emailET.setError("Please enter valid email address.");
                emailET.requestFocus();
            } else if (TextUtils.isEmpty(passET.getText().toString().trim()) || passET.getText().toString().trim().length() < 7) {
                passET.setError("Please enter password of minimum 7 characters.");
                passET.requestFocus();
            } else {
                doLogin();
//            Intent intent = new Intent(this, HomeActivity.class);
//            startActivity(intent);
//            finish();
//            Toast.makeText(this, "Under development....", Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        if (i == R.id.customerRB) {
            USER_TYPE = "Member";
        } else if (i == R.id.authorRB) {
            USER_TYPE = "Author";
        }
    }

    /////////// Facebook //////////////


    public void fbadd() {
        //        callbackManager = CallbackManager.Factory.create();
        /////////////////////////////////////////
        keyHash();
        mCallbackManager = CallbackManager.Factory.create();
        //   LoginManager.getInstance().logOut();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                //  textView.setText(displayMessage(profile1));
                // System.out.println(displayMessage(profile1));
            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(mCallbackManager, mFacebookCallback);
//        LoginManager.getInstance().ppublish(feed, true, onPublishListener);
        //////////////////////////////////////////////
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("hnweb.com.ekasst", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();


            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.e("LoginActivity Response ", response.toString());
                            System.out.println("arshres" + response.toString());

                            try {
                                String id = object.getString("id");
                                String Name = object.getString("name");
                                String FEmail = "";
                                if (!TextUtils.isEmpty(object.getString("email"))) {
                                    FEmail = object.getString("email");
                                }
                                String profilePicUrl = "";
                                Log.e("Email = ", id + "  " + FEmail + "  " + Name);
                                if (object.has("picture")) {
                                    profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    Log.e("Profile", profilePicUrl);
                                }

                                doFacebookLogin(id, Name, FEmail, profilePicUrl);
                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
//                                if (CheckConnectivity.checkInternetConnection(SignInActivity.this)) {
////                                sharePhotoToFacebook();
//                                    fbSignInWebservice(id, Name, FEmail);
//                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday,picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();


            //textView.setText(displayMessage(profile));
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 140) {
////            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
//        } else {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//        }


    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //   textView.setText(displayMessage(profile));
    }

    @Override
    public void onStop() {

        try {
            super.onStop();
            profileTracker.stopTracking();
            tokenTracker.stopTracking();
        } catch (Exception e) {
//            e.printStackTrace();
        }

    }

    public void getFeeds() {
        String appToken = "1408024832568070" + "|" + "0cd1e113b697c39f82fe2217872c50d6";
        AccessToken accessToken = new AccessToken("1408024832568070|e38P-8TN3WJN_IN9HPhf5_shBrs", "1408024832568070", "SCBEC", null, null, null, null, null);
        Log.e("TOKEN", accessToken.getToken());
        accessToken.getPermissions();
//        for (int j = 0; j < accessToken.getPermissions().size(); j++) {
//            Log.e("TOKEN", String.valueOf(accessToken.getPermissions(j)));
//        }

//        GraphRequest request = GraphRequest.newGraphPathRequest(
//                accessToken,
//                "/SCBEC/posts",
//                new GraphRequest.Callback() {
//                    @Override
//                    public void onCompleted(GraphResponse response) {
//                        // Insert your code here
//                        Log.e("REPONSE", response.toString());
//                    }
//                });
//
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "attachments");
//        request.setParameters(parameters);
//        request.executeAsync();

//        Session session = Session.getActiveSession();
//        Request request = new Request(session, "me/albums");
//
//        Response response = request.executeAndWait();

        GraphRequest request = GraphRequest.newGraphPathRequest(
                accessToken,
                "/hnmike222/user_posts",
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        // Insert your code here
                        Log.e("REPONSE", response.toString());
                    }
                });
///posts?fields=attachments
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "data");
//        request.setParameters(parameters);
        request.executeAsync();
    }
}
