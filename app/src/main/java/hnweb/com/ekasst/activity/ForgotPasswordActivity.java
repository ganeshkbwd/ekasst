package hnweb.com.ekasst.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.application.IResult;
import hnweb.com.ekasst.application.MyVolleyService;
import hnweb.com.ekasst.utils.AppAPI;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailET;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        emailET = (EditText) findViewById(R.id.emailET);
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendBTN:
                if (!Patterns.EMAIL_ADDRESS.matcher(emailET.getText().toString().trim()).matches()) {
                    emailET.setError("Please enter email address.");
                    emailET.requestFocus();
                } else {
                    doForgotPasword();
//                    Toast.makeText(this, "Under Development.....", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void doForgotPasword() {
        Map<String, String> params = new HashMap<>();

        params.put("email_address", emailET.getText().toString().trim());
        mVolleyService.postDataVolley("POSTCALL", AppAPI.FORGOT_PASSWORD, params, "forgot_password");
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (meg_code==1){
                        Intent intent = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(ForgotPasswordActivity.this, "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

}
