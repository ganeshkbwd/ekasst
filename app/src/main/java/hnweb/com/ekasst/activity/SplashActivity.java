package hnweb.com.ekasst.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hnweb.com.ekasst.R;
import hnweb.com.ekasst.spreadsheet.PostDataTask;
import hnweb.com.ekasst.spreadsheet.SpreadSheetTask;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//go next

                if (sharedPreferences.getInt("USER_ID", 0) == 0) {
                    Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("Member")) {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
//                Toast.makeText(this, "Under Development...", Toast.LENGTH_SHORT).show();
                    }
                }
//                            goNextScreen();
            }
        }, 4000);

        PostDataTask postDataTask = new PostDataTask();
//execute asynctask
        postDataTask.execute(SpreadSheetTask.URL, SpreadSheetTask.deviceInfo(SplashActivity.this));
    }

    @Override
    public void onClick(View view) {
        if (sharedPreferences.getInt("USER_ID", 0) == 0) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        } else {
            if (sharedPreferences.getString("USER_TYPE", "").equalsIgnoreCase("Customer")) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
//                Toast.makeText(this, "Under Development...", Toast.LENGTH_SHORT).show();
            }
        }


    }
}
